using System;
using Xunit;
using CSBlogModel;
using Bloger = CSBlogModel.Bloger;
using System.Linq;

namespace CSBlogTest
{
    public class BlogerModel
    {
        public class BlogerT
        {
            // สร้าง Bloger
            [Fact]
            public void FirstCreateNotNull()
            {
                Bloger bloger = new Bloger();
                Assert.NotNull(bloger);
            }
        }

        public class ArticleT
        {
            // เพิ่มบทความไม่ม่ค่า
            [Fact]
            public void FirstCreateIsNull()
            {
                Bloger bloger = new Bloger();
                Assert.Null(bloger.Blogs);
            }

            // เพิ่มบทความ
            [Fact]
            public void FirstCreateNotNull()
            {
                Bloger bloger = new Bloger();
                bloger.AddBlog(new ArticleRes());
                Assert.NotNull(bloger.Blogs);
            }
        }

        public class ProfileT
        {
            // สร้างโปรไฟล์แล้วมีโปรไฟล์
            [Fact]
            public void FirstCreateNotNull()
            {
                Bloger bloger = new Bloger();
                Assert.NotNull(bloger.Profile);
            }

            // อับเดจโปรไฟล์
            [Fact]
            public void Update()
            {
                Bloger bloger = new Bloger();
                //bloger.Profile = new Profile();
                bloger.Profile.UpdateProfile("codename", "bio", "desc");
                Assert.True(bloger.Profile.CodeName == "codename");
                Assert.True(bloger.Profile.Bio == "bio");
                Assert.True(bloger.Profile.Description == "desc");
            }
        }
    }
}
