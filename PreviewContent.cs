﻿using System;
using System.Collections.Generic;
using CSBlogModel;
using Xunit;

namespace CSBlogTest
{
    public class PreviewContentTest
    {
        [Fact]
        public void FirstNewPreviewContentIsNotNull()
        {
            PreviewContent previewContent = new PreviewContent("", new List<Tag>(), "");
            Assert.NotNull(previewContent);
        }

        [Fact]
        public void FirstNewPreviewContentCheckValueEquelTure()
        {
            PreviewContent previewContent = new PreviewContent("Titile", new List<Tag>() { new Tag() { Name = "tag" } }, "PreviewImage");
            Assert.True(previewContent.Title == "Titile");
            Assert.True(previewContent.PreviewImage == "PreviewImage");
            Assert.True(previewContent.Tags[0].Name == "tag");
        }

        [Fact]
        public void FirstNewPreviewContentCheckValueEquelFalse()
        {
            PreviewContent previewContent = new PreviewContent("Titile", new List<Tag>() { new Tag() { Name = "tag" } }, "PreviewImage");
            Assert.False(previewContent.Title == "");
            Assert.False(previewContent.PreviewImage == "");
            Assert.False(previewContent.Tags[0].Name == "false");
        }

        [Fact]
        public void FirstNewPreviewContentCheckBackgroupEquelTrue()
        {
            PreviewContent previewContent = new PreviewContent("Titile", new List<Tag>(), "PreviewImage");
            previewContent.ChangeBackgroupImage("ImgUrl");
            Assert.NotNull(previewContent.PreviewImage);
        }
    }
}
