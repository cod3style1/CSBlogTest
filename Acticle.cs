﻿using System;
using CSBlogModel;
using System.Collections.Generic;
using Xunit;

namespace CSBlogTest
{
    public class ActicleTest
    {
        [Fact]
        public void FirstNewActicle()
        {
            Article acticle = new Article("titile", new List<Tag> { },  "subtitle");
            Assert.NotNull(acticle);
        }

        [Fact]
        public void FirstNewActicleCheckCreateDateNotNull()
        {
            Article acticle = new Article("titile", new List<Tag> { }, "subtitle");
            Assert.NotNull(acticle.CreateAt);
            Assert.NotNull(acticle.UpdateAt);
        }

        [Fact]
        public void FirstNewActicleCheckCreateDateEquelNow()
        {
            Article acticle = new Article("titile", new List<Tag> { }, "subtitle");
            Assert.True(acticle.CreateAt.Date == DateTime.Now.Date);
            Assert.True(acticle.UpdateAt.Date == DateTime.Now.Date);
        }

        public class Title
        {
            [Fact]
            public void EditTitleCheckEquelTure()
            {
                Article acticle = new Article("titile", new List<Tag> { }, "subtitle");
                acticle.EditTitle("EditTitle");
                Assert.True(acticle.Title == "EditTitle");
            }

            [Fact]
            public void EditTitleIsNull()
            {
                Article acticle = new Article("titile", new List<Tag> { }, "subtitle");
                Assert.ThrowsAny<Exception>(() =>
                {
                    acticle.EditTitle(null);
                });
            }
        }

        public class SubTitle
        {
            [Fact]
            public void EditTitleCheckEquelTure()
            {
                Article acticle = new Article("titile", new List<Tag> { }, "subtitle");
                acticle.EditSubTitle("EditSubTitle");
                Assert.True(acticle.Subtitle == "EditSubTitle");
            }

            [Fact]
            public void EditTitleIsNull()
            {
                Article acticle = new Article("titile", new List<Tag> { }, "subtitle");
                Assert.ThrowsAny<Exception>(() =>
                {
                    acticle.EditSubTitle(null);
                });
            }
        }

        public class Paragraphs
        {
            [Fact]
            public void UpdateNullIsException()
            {
                Article acticle = new Article("titile", new List<Tag> { }, "subtitle");
                Assert.ThrowsAny<Exception>(() =>
                {
                    acticle.UpdateParagraphs(null);
                });
            }
        }
    }
}
