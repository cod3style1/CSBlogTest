﻿using System;
using CSBlogModel;
using Xunit;

namespace CSBlogTest
{
    public class SubscribeTest
    {
        public class Follow
        {
            [Fact]
            public void AddFollowIsTrue()
            {
                Subscribe subscribe = new Subscribe();
                Guid BlogerID = Guid.NewGuid();
                Guid FollowID = Guid.NewGuid();

                subscribe.FollowBloger(BlogerID, FollowID);

                Assert.True(BlogerID == subscribe.BlogerID);
                Assert.True(FollowID == subscribe.FollowID);
            }


            [Fact]
            public void AddFollowIsException()
            {
                Subscribe subscribe = new Subscribe();
                var BlogerID = Guid.Empty;
                Guid FollowID = Guid.NewGuid();

                Assert.Throws<Exception>(() => {
                    subscribe.FollowBloger(BlogerID, FollowID);
                });
            }
        } 
    }
}
