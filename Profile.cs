﻿using System;
using CSBlogModel;
using Xunit;

namespace CSBlogTest
{
    public class ProfileTest
    {
        public class ProfileT
        {

            [Fact]
            public void FirstNewCheckDetailIsTrue()
            {
                Profile profile = new Profile("codename", "bio", "desc");

                Assert.True(profile.ProfileID != null);
                Assert.True(profile.CodeName == "codename");
                Assert.True(profile.Bio == "bio");
                Assert.True(profile.Description == "desc");
            }

            [Fact]
            public void Update()
            {
                Profile profile = new Profile("codename", "bio", "desc");
                profile.UpdateProfile("codenameUpdate", "bioUpdate", "descUpdate");

                Assert.True(profile.ProfileID != null);
                Assert.True(profile.CodeName == "codenameUpdate");
                Assert.True(profile.Bio == "bioUpdate");
                Assert.True(profile.Description == "descUpdate");
            }

            [Fact]
            public void UpdateCheckNullPropertyIsFalse()
            {
                Profile profile = new Profile("codename", "bio", "desc");
                profile.UpdateProfile(null, null, null);

                Assert.False(profile.CodeName == null);
                Assert.False(profile.Bio == null);
                Assert.False(profile.Description == null);
            }

        }
    }
}
